package com.example.kfuV1.controller;

import com.example.kfuV1.appuser.AppUser;
import com.example.kfuV1.appuser.AppUserRole;
import lombok.Data;
import org.springframework.context.annotation.Bean;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserForWeb {
    private Long id;
    @NotEmpty(message = "Name should not be empty")
    @Size(min=2, max=30, message = "Name should be between 2 and 30 characters")
    private String name;
    @NotEmpty(message = "Username should not be empty")
    @Size(min=2, max=30, message = "Username should be between 2 and 30 characters")
    private String username;
    @NotEmpty(message = "Name should not be empty")
    @Email(message = "Email should be valid")
    private String email;

    public UserForWeb() {
    }

    public UserForWeb(AppUser appUser){
        this.id=appUser.getId();
        this.name=appUser.getName();
        this.username=appUser.getUsernameNotEmail();
        this.email=appUser.getEmail();
    }

    public static UserForWeb createUser(AppUser appUser){
       UserForWeb user = new UserForWeb();
       user.name = appUser.getName();
       user.username = appUser.getUsernameNotEmail();
       user.id = appUser.getId();
       user.email = appUser.getEmail();
       return user;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'';
    }
}
