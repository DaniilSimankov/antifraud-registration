package com.example.kfuV1.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Configuration
@RequestMapping("/api/v1/auth")
public class AuthController {

    @GetMapping("/login")
    public String getLoginPage(){
        return "auth/login";
    }

    @GetMapping("/success")
    public String getSuccessPage(){
        return "auth/success";
    }
}
