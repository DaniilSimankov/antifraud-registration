package com.example.kfuV1.controller;

import com.example.kfuV1.appuser.AppUser;
import com.example.kfuV1.appuser.AppUserRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/v1/users")
public class AllUserController {

    private final AppUserRepository appUserRepository;

    public AllUserController(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute("users", appUserRepository.findAll());
        return "users/users";
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable Long id, Model model) {
        model.addAttribute("user", appUserRepository.findById(id).orElseThrow(() -> {
            throw new UsernameNotFoundException("No user with this id");
        }));
        return "users/user_by_id";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") Long id) {
        model.addAttribute("user",
                UserForWeb.createUser(appUserRepository.findById(id).orElseThrow(() -> {
                    throw new UsernameNotFoundException("No user with this id");
                })));
        return "users/edit";
    }

    @PostMapping("/{id}/edit")
    public String update(@PathVariable("id") Long id, @ModelAttribute("user") @Valid UserForWeb user,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "users/edit";
        appUserRepository.updateInformationAppUser(
                    id,
                    user.getName(),
                    user.getUsername(),
                    user.getEmail()
            );
        return "redirect:/api/v1/users";
    }

    @PostMapping("/{id}")
    public String delete(@PathVariable("id") Long id) {
        appUserRepository.deleteTokenById(id);
        appUserRepository.deleteById(id);
        return "redirect:/api/v1/users";
    }
}


