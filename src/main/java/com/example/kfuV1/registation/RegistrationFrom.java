package com.example.kfuV1.registation;

import com.example.kfuV1.appuser.AppUser;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;

@Controller
@RequestMapping(path = "api/v1/registration")
@AllArgsConstructor
public class RegistrationFrom {
    private final RegistrationService registrationService;

    @GetMapping
    public String regForm(@ModelAttribute("user") AppUser appUser) {
        return "registration/new";
    }

    @PostMapping
    public String register(Model model, @ModelAttribute("user") @Valid AppUser appUser,
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "registration/new";
        RegistrationRequest registrationRequest = new RegistrationRequest(
                appUser.getName(),
                appUser.getUsernameNotEmail(),
                appUser.getEmail(),
                appUser.getPassword()
        );
        String token = registrationService.register(registrationRequest);

        model.addAttribute("token", token);
        return "registration/registrationByEmail";
    }

    @GetMapping("confirm")
    public String confirm(@RequestParam("token") String token) {
        registrationService.confirmToken(token);
        return "registration/confirm";
    }

}
