package com.example.kfuV1.registation.token;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface ConfirmationTokenRepository
        extends JpaRepository<ConfirmationToken, Long> {

    //language=SQL
    public static final String UPDATE_QUERY_CONFIRMED ="UPDATE ConfirmationToken c SET c.confirmedAt = ?2 WHERE c.token = ?1";

    Optional<ConfirmationToken> findByToken(String token);

    @Transactional
    @Modifying
    @Query(UPDATE_QUERY_CONFIRMED)
    int updateConfirmedAt(String token,
                          LocalDateTime confirmedAt);
}
