package com.example.kfuV1.registation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Service
@AllArgsConstructor
public class FraudService {

    private static final int CONNECTION_TIMEOUT = 5000;

    public static String getFraudByUsername(String name1, String name2, String username1, String username2, String email1, String email2) {
        try {
            final URL url = new URL("http://localhost:8081/similarity/stand-reg?name1=" + name1 + "l&name2=" + name2 +
                    "&surname1=" + username1 + "&surname2=" + username2 +
                    "&login1=" + email1 + "&login2=" + email2);
            final HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(CONNECTION_TIMEOUT);
            con.setReadTimeout(CONNECTION_TIMEOUT);

            try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                final StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                return content.toString();
            } catch (final Exception ex) {
                ex.printStackTrace();
                return "";
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
