package com.example.kfuV1.appuser;

import com.example.kfuV1.registation.FraudService;
import com.example.kfuV1.registation.token.ConfirmationToken;
import com.example.kfuV1.registation.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.postgresql.util.PSQLException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {

    public static final String USER_NOT_FOUND_MESSAGE =
            "user with email \"%s\" not fount";

    private final AppUserRepository appUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        return appUserRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException(
                                String.format(USER_NOT_FOUND_MESSAGE, email)
                        )
                );
    }

    public String signUpUser(AppUser appUser) {
        boolean userExists = appUserRepository
                .findByEmail(appUser.getEmail())
                .isPresent();

        if (userExists && appUser.isEnabled()) {
            //TODO: check of attribute are the same and
            //TODO: if email not confirmed send confirmation email
            throw new IllegalStateException("email already taken");
        }


        String encodedPassword = bCryptPasswordEncoder
                .encode(appUser.getPassword());

        appUser.setPassword(encodedPassword);

        double fraud;
        for (AppUser user : appUserRepository.findAll()) {
            fraud = Double.parseDouble(
                    FraudService.getFraudByUsername(
                            appUser.getName(), user.getName(),
                            appUser.getUsernameNotEmail(), user.getUsernameNotEmail(),
                            appUser.getEmail(), user.getEmail()
                    )
            );
            if (fraud > 0.7) {
                appUserRepository.fraudAppUser(user.getEmail());
                appUser.setFraud(1);
            }
        }

        try {
            appUserRepository.save(appUser);
        } catch (Exception e){
            throw new IllegalArgumentException("Registration form filled out incorrectly");
        }

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                appUser
        );

        confirmationTokenService.saveConfirmationToken(confirmationToken);

        //TO DO: SEND EMAIL

        return token;
    }

    public int enableAppUser(String email) {
        return appUserRepository.enableAppUser(email);
    }
}
