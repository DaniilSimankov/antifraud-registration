package com.example.kfuV1.appuser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    //language=SQL
    public static final String UPDATE_QUERY_ENABLE = "UPDATE AppUser a SET a.enabled = TRUE WHERE a.email = ?1";
    //language=SQL
    public static final String UPDATE_QUERY_FRAUD = "UPDATE AppUser a SET a.fraud = 1 WHERE a.email = ?1";
    //language=SQL
    public static final String UPDATE_QUERY_USER ="UPDATE AppUser a SET a.name = ?2, a.username = ?3, a.email = ?4 WHERE a.id = ?1";
    //language=SQL
    public static final String DELETE_QUERY_TOKEN = "delete from ConfirmationToken c where c.id=?1";
    //language=SQL
    public static final String DELETE_QUERY_USER = "DELETE FROM AppUser a WHERE a.id=?1";

    Optional<AppUser> findByEmail(String email);

    @Transactional
    @Modifying
    @Query(UPDATE_QUERY_ENABLE)
    int enableAppUser(String email);

    @Transactional
    @Modifying
    @Query(UPDATE_QUERY_FRAUD)
    int fraudAppUser(String email);

    @Transactional
    @Modifying
    @Query(UPDATE_QUERY_USER)
    int updateInformationAppUser(Long id, String name, String username, String email);

    @Transactional
    @Modifying
    @Query(DELETE_QUERY_TOKEN)
    void deleteTokenById(Long id);

    @Transactional
    @Modifying
    @Query(DELETE_QUERY_USER)
    void deleteById(Long id);
}

//TO DO сделать нормальное удаление