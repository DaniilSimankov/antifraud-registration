package com.example.kfuV1.email;

public interface EmailSender {

    void send(String to, String email);
}
