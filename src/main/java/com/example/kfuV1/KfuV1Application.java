package com.example.kfuV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KfuV1Application {

	public static void main(String[] args) {
		SpringApplication.run(KfuV1Application.class, args);
	}

}
